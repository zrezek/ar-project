using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class karta{
    	public bool Visible;
    	public int Value;
    	public string Color;
    	public string Name;

    public karta(bool visible, int value, string color, string name){
        Visible = visible;
        Value = value;
        Color = color;
        Name = name;
    }
}
    
    
 
public class karteScript : MonoBehaviour
{
    public Text textElement;

    //Inicializacija spremenljivk
    //TWO
    karta two_of_clubs = new karta(false,2,"clubs","two");
    karta two_of_hearts = new karta(false,2,"hearts","two");
    karta two_of_diamonds = new karta(false,2,"diamonds","two");
    karta two_of_spades = new karta(false,2,"spades","two");
    //THREE
    karta three_of_clubs = new karta(false,3,"clubs","three");
    karta three_of_hearts = new karta(false,3,"hearts","three");
    karta three_of_diamonds = new karta(false,3,"diamonds","three");
    karta three_of_spades = new karta(false,3,"spades","three");
    //FOUR
    karta four_of_clubs = new karta(false,4,"clubs","four");
    karta four_of_hearts = new karta(false,4,"hearts","four");
    karta four_of_diamonds = new karta(false,4,"diamonds","four");
    karta four_of_spades = new karta(false,4,"spades","four");
    //FIVE
    karta five_of_clubs = new karta(false,5,"clubs","five");
    karta five_of_hearts = new karta(false,5,"hearts","five");
    karta five_of_diamonds = new karta(false,5,"diamonds","five");
    karta five_of_spades = new karta(false,5,"spades","five");
    //six
    karta six_of_clubs = new karta(false,6,"clubs","six");
    karta six_of_hearts = new karta(false,6,"hearts","six");
    karta six_of_diamonds = new karta(false,6,"diamonds","six");
    karta six_of_spades = new karta(false,6,"spades","six");
    //seven
    karta seven_of_clubs = new karta(false,7,"clubs","seven");
    karta seven_of_hearts = new karta(false,7,"hearts","seven");
    karta seven_of_diamonds = new karta(false,7,"diamonds","seven");
    karta seven_of_spades = new karta(false,7,"spades","seven");
    //eight
    karta eight_of_clubs = new karta(false,8,"clubs","eight");
    karta eight_of_hearts = new karta(false,8,"hearts","eight");
    karta eight_of_diamonds = new karta(false,8,"diamonds","eight");
    karta eight_of_spades = new karta(false,8,"spades","eight");
    //nine
    karta nine_of_clubs = new karta(false,9,"clubs","nine");
    karta nine_of_hearts = new karta(false,9,"hearts","nine");
    karta nine_of_diamonds = new karta(false,9,"diamonds","nine");
    karta nine_of_spades = new karta(false,9,"spades","nine");
    //ten
    karta ten_of_clubs = new karta(false,10,"clubs","ten");
    karta ten_of_hearts = new karta(false,10,"hearts","ten");
    karta ten_of_diamonds = new karta(false,10,"diamonds","ten");
    karta ten_of_spades = new karta(false,10,"spades","ten");
    //jack
    karta jack_of_clubs = new karta(false,11,"clubs","jack");
    karta jack_of_hearts = new karta(false,11,"hearts","jack");
    karta jack_of_diamonds = new karta(false,11,"diamonds","jack");
    karta jack_of_spades = new karta(false,11,"spades","jack");
    //queen
    karta queen_of_clubs = new karta(false,12,"clubs","queen");
    karta queen_of_hearts = new karta(false,12,"hearts","queen");
    karta queen_of_diamonds = new karta(false,12,"diamonds","queen");
    karta queen_of_spades = new karta(false,12,"spades","queen");
    //king
    karta king_of_clubs = new karta(false,13,"clubs","king");
    karta king_of_hearts = new karta(false,13,"hearts","king");
    karta king_of_diamonds = new karta(false,13,"diamonds","king");
    karta king_of_spades = new karta(false,13,"spades","king");
    //ace
    karta ace_of_clubs = new karta(false,14,"clubs","ace");
    karta ace_of_hearts = new karta(false,14,"hearts","ace");
    karta ace_of_diamonds = new karta(false,14,"diamonds","ace");
    karta ace_of_spades = new karta(false,14,"spades","ace");

    karta[] hand = new karta[7];

    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public int countElements(karta[] hand){
        int counter = 0;
        for(int i = 0; i < 7; i++){
            if(hand[i] != null){
                counter++;
            }
        }
        return counter;
    }

    public void update_karta(karta imekarte){
    	bool exists = false;
    	for(int i = 0; i < hand.Length; i++){
    		if (hand[i] == imekarte){
    			exists = true;
    		}
    	}
        if(exists == false){
        	for(int i = 0; i < hand.Length; i++){
        		if(hand[i] == null){
        			hand[i] = imekarte;
        			break;
        		}
        	}
        }
    }

    public void reset_hand(){
        for(int i = 0; i < 7; i++){
            hand[i] = null;
        }
        print("Hand has been reset");
    }
    
    public void checkhand(){
        int handsize = countElements(hand);
        //POKER
    
        int najvecjiPoker = 0;

        for(int i = 0; i < handsize; i++){
             for(int j = 0; j < handsize; j++){
                for(int k = 0; k < handsize; k++){
                    for(int l = 0; l < handsize; l++){
                        if(hand[i] != hand[j] && hand[i] != hand[k] && hand[j] != hand[k] && hand[i] != hand[l] && hand[j] != hand[l] && hand[k] != hand[l] ){
                            if (hand[i].Value == hand[j].Value && hand[i].Value == hand[k].Value && hand[i].Value == hand[l].Value){
                                najvecjiPoker = hand[i].Value;
                            }
                        }
                    }
                }
            }
        }
        print("Poker: " + najvecjiPoker);
        

        //TRIS
        int najvecjiTris = 0;
        for(int i = 0; i < handsize; i++){
             for(int j = 0; j < handsize; j++){
                for(int k = 0; k < handsize; k++){
                    if(hand[i] != hand[j] && hand[i] != hand[k] && hand[j] != hand[k]){
                        if (hand[i].Value == hand[j].Value && hand[i].Value == hand[k].Value){
                            if(hand[i].Value > najvecjiTris && hand[i].Value != najvecjiPoker){
                                najvecjiTris = hand[i].Value;
                            }
                        }
                    }
                }
            }
        }
        print("Tris: " + najvecjiTris);

        //PAR
        int najvecjiPar = 0;
        for(int i = 0; i < handsize; i++){
             for(int j = 0; j < handsize; j++){
                if(hand[i] != hand[j]){
                    if (hand[i].Value == hand[j].Value ){
                        if(hand[i].Value > najvecjiPar && hand[i].Value != najvecjiTris && hand[i].Value != najvecjiPoker ){
                            najvecjiPar = hand[i].Value;
                        }
                    }
                }
            }
        }
        print("Pair: " + najvecjiPar);

        //FULL HOUSE
        bool fullHouse = false;
        if(najvecjiPar != 0 && najvecjiTris != 0){
            fullHouse = true;
            print("Full house: Tris - " + najvecjiTris + "Par - " + najvecjiPar);
        }

        //LESTVICA 
        int[] lestvica = new int[handsize];
        int counter = 0;
        int najvecjiLestvica = 0;
        for(int i = 0;i < handsize; i++){
            lestvica[i] = hand[i].Value;
        }
        System.Array.Sort(lestvica);
        for(int i = 1; i < handsize; i++){
            if(lestvica[i-1] == (lestvica[i])){
                counter++;
            }else if(counter >= 5){
                najvecjiLestvica = lestvica[i-1];
            }else{
                counter = 0;
            }
        }

        print("Lestvica: " + najvecjiLestvica);

        //FLUSH
        int hearts = 0;
        int clubs = 0;
        int spades = 0;
        int diamonds = 0;
        string barva = null;

        for(int i = 0; i < handsize; i++){
            if(hand[i].Color == "hearts"){
                hearts++;
            }else if(hand[i].Color == "clubs"){
                clubs++;
            }else if(hand[i].Color == "spades"){
                spades++;
            }else{
                diamonds++;
            }
        }

        if(hearts >= 5){
                barva = "Hearts";
                print("Flush: Hearts");
            }else if(clubs >= 5){
                barva = "Clubs";
                print("Flush: Clubs");
            }else if(spades >= 5){
                barva = "Spades";
                print("Flush: Spades");
            }else if(diamonds >= 5){
                barva = "Diamonds";
                print("Flush: Diamonds");
        }

        if(najvecjiPoker != 0){
            print("Najvecja kombinacija: Poker " + najvecjiPoker);
        }else if(fullHouse == true){
            print("Najvecja kombinacija: Full house");
        }else if(barva != null){
            print("Najvecja kombinacija: Barva " + barva);
        }else if(najvecjiLestvica != 0){
            int najmanjsiLestvica = najvecjiLestvica-5;
            print("Najvecja kombinacija: Lestvica od " + najmanjsiLestvica + "do " + lestvica);
        }else if(najvecjiTris != 0){
            print("Najvecji kombinacija: Tris " + najvecjiTris);
        }else if(najvecjiPar != 0){
            print("Najvecja kombinacija: Par " + najvecjiPar);
        }else{
            print("Ni kombinacij!");
        }
    }

    //TWO
    public void update_two_of_clubs(){
     update_karta(two_of_clubs);
    }

    public void update_two_of_hearts(){
     update_karta(two_of_hearts);
    }

    public void update_two_of_diamonds(){
     update_karta(two_of_diamonds);
    }

    public void update_two_of_spades(){
     update_karta(two_of_spades);
    }
    
    //THREE
    public void update_three_of_clubs(){
     update_karta(three_of_clubs);
    }

    public void update_three_of_hearts(){
     update_karta(three_of_hearts);
    }

    public void update_three_of_diamonds(){
     update_karta(three_of_diamonds);
    }

    public void update_three_of_spades(){
     update_karta(three_of_spades);
    }
    
    //FOUR
    public void update_four_of_clubs(){
     update_karta(four_of_clubs);
    }

    public void update_four_of_hearts(){
     update_karta(four_of_hearts);
    }

    public void update_four_of_diamonds(){
     update_karta(four_of_diamonds);
    }

    public void update_four_of_spades(){
     update_karta(four_of_spades);
    }

    //FIVE
    public void update_five_of_clubs(){
     update_karta(five_of_clubs);
    }

    public void update_five_of_hearts(){
     update_karta(five_of_hearts);
    }

    public void update_five_of_diamonds(){
     update_karta(five_of_diamonds);
    }

    public void update_five_of_spades(){
     update_karta(five_of_spades);
    }

    //SIX
    public void update_six_of_clubs(){
     update_karta(six_of_clubs);
    }

    public void update_six_of_hearts(){
     update_karta(six_of_hearts);
    }

    public void update_six_of_diamonds(){
     update_karta(six_of_diamonds);
    }

    public void update_six_of_spades(){
     update_karta(six_of_spades);
    }
    
    //SEVEN
    public void update_seven_of_clubs(){
     update_karta(seven_of_clubs);
    }

    public void update_seven_of_hearts(){
     update_karta(seven_of_hearts);
    }

    public void update_seven_of_diamonds(){
     update_karta(seven_of_diamonds);
    }

    public void update_seven_of_spades(){
     update_karta(seven_of_spades);
    }

    //EIGHT
    public void update_eight_of_clubs(){
     update_karta(eight_of_clubs);
    }

    public void update_eight_of_hearts(){
     update_karta(eight_of_hearts);
    }

    public void update_eight_of_diamonds(){
     update_karta(eight_of_diamonds);
    }
    public void update_eight_of_spades(){
     update_karta(eight_of_spades);
    }

    //NINE
    public void update_nine_of_clubs(){
     update_karta(nine_of_clubs);
    }

    public void update_nine_of_hearts(){
     update_karta(nine_of_hearts);
    }

    public void update_nine_of_diamonds(){
     update_karta(nine_of_diamonds);
    }

    public void update_nine_of_spades(){
     update_karta(nine_of_spades);
    }

    //TEN
    public void update_ten_of_clubs(){
     update_karta(ten_of_clubs);
    }

    public void update_ten_of_hearts(){
     update_karta(ten_of_hearts);
    }

    public void update_ten_of_diamonds(){
     update_karta(ten_of_diamonds);
    }

    public void update_ten_of_spades(){
     update_karta(ten_of_spades);
    }
    
    //JACK
    public void update_jack_of_clubs(){
     update_karta(jack_of_clubs);
    }

    public void update_jack_of_hearts(){
     update_karta(jack_of_hearts);
    }

    public void update_jack_of_diamonds(){
     update_karta(jack_of_diamonds);
    }

    public void update_jack_of_spades(){
     update_karta(jack_of_spades);
    }

    //QUEEN
    public void update_queen_of_clubs(){
     update_karta(queen_of_clubs);
    }

    public void update_queen_of_hearts(){
     update_karta(queen_of_hearts);
    }

    public void update_queen_of_diamonds(){
     update_karta(queen_of_diamonds);
    }

    public void update_queen_of_spades(){
     update_karta(queen_of_spades);
    }

    //KING
    public void update_king_of_clubs(){
     update_karta(king_of_clubs);
    }

    public void update_king_of_hearts(){
     update_karta(king_of_hearts);
    }

    public void update_king_of_diamonds(){
     update_karta(king_of_diamonds);
    }

    public void update_king_of_spades(){
     update_karta(king_of_spades);
    }
    
    //ACE
    public void update_ace_of_clubs(){
     update_karta(ace_of_clubs);
    }

    public void update_ace_of_hearts(){
     update_karta(ace_of_hearts);
    }

    public void update_ace_of_diamonds(){
     update_karta(ace_of_diamonds);
    }

    public void update_ace_of_spades(){
     update_karta(ace_of_spades);
    }
    

    }
    
