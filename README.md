To run the program install apk on your android device and run it.
Cards need to be scanned slowly. If a checkmark is visible card was scanned successfully. 
You can scan up to 7 different cards (2 in hand, 5 on table). 
If you do not need scanned cards anymore, press “Reset” button to reset and delete all scanned cards. 
If you want to output combinations, press “Print” button.

------------------------------------------------------------------------------------

In general, poker is quite a hard game to learn for beginners. We decided to solve this problem by creating an application that would help new players by showing them card combinations and thus helping them while playing poker for the first time.

In a game of poker each player gets dealt two cards that he later combines with the other 5 cards that are put on the table. The game is played with a 52-card pack. Our application will allow players to scan the cards on the table with the help of augmented reality. After scanning the cards, the player will be shown his best combination of cards (best hand). There will also be a button that allows the player to see a ranking of poker hands from best to worst. 